# Electric Station   

this project is my assignment(Farhad Yousefi) to begin one of the Devolon Team members  

## Project Description
implementing Rest-API for the electric vehicle charging station management system.

### Notes:

* Pay attention to the scalability of the API.
* One charging company can own one or more other charging companies.
* the parent company should have the access to all the child company's stations hierarchically.

#### database schema:
* Station (id, name, latitude, longitude, company_id)
* Company (id, parent_company_id, name)

#### Tasks
* Api should support CRUD for stations and companies.
* Implement endpoint which gets all stations.Within the radius of n kilometers 
  from a point (latitude, longitude) ordered by distance.
  Including all the children stations in the tree, for the given company_id.
* Write a simple, not fancy interface that will consume your API programmatically.
* TDD

### development assumptions 
```
* I used java(version 14) as a language and spring as the main framework
* There was no limitation of using a specific database in assignment so because of Ltree data type, I used PostgreSQL
* This project will create DEVOLON schema on the default database (Postgres)
* application context path and port is '/electricStation' and '8989' respectively
* You can find export of Postman file ('electric station.postman_collection.json') in 'resources/' path
* simple clients of both companies and stations API are in the client package
* I assume you want to call API from your local machine
```

### Sample API endpoint
* company related API
```
POST http://localhost:8989/electricStation/companies/1/stations
GET http://localhost:8989/electricStation/companies/1
POST http://localhost:8989/electricStation/companies/
PUT http://localhost:8989/electricStation/companies/1
DELETE http://localhost:8989/electricStation/companies/5
```
* station related API
```
GET http://localhost:8989/electricStation/stations/1
POST http://localhost:8989/electricStation/stations/
PUT http://localhost:8989/electricStation/stations/1
DELETE http://localhost:8989/electricStation/stations/1
```
### Authors

* **Farhad Yousefi**  - [Farhad](https://github.com/farhadHM/)


