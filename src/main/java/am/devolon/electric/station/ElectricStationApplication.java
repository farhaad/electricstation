package am.devolon.electric.station;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * created by farhad (farhad.yousefi@outlook.com) on 3/23/2021 AD
 */

@SpringBootApplication
public class ElectricStationApplication {
    public static void main(String[] args) {
        SpringApplication.run(ElectricStationApplication.class, args);
    }

}
