package am.devolon.electric.station.client;

import am.devolon.electric.station.model.AllStationsRequest;
import am.devolon.electric.station.model.Company;
import am.devolon.electric.station.model.Station;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * created by farhad (farhad.yousefi@outlook.com) on 3/24/2021 AD
 */
public class CompanyServicesClient {
    private static final Logger LOGGER = LogManager.getLogger(CompanyServicesClient.class);
    private static final String URL = "http://localhost:8989/electricStation/companies/";

    public static void main(String[] args) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        callGetAllStationInsideRadius(restTemplate);
        callGetCompanyById(restTemplate);
        callSaveCompany(restTemplate);
        callUpdateCompany(restTemplate);
        callRemoveCompany(restTemplate);
    }

    private static void callGetAllStationInsideRadius(RestTemplate restTemplate) {
        LOGGER.info("calling getAllStationInsideRadius service");

        AllStationsRequest criteria = new AllStationsRequest(34.210, 10.3, 300);
        Map<String, String> params = new HashMap<>();
        params.put("id", "1");
        List<Station> response = restTemplate.postForObject(
                URL + "{id}/stations",
                criteria,
                List.class,
                params);

        LOGGER.info(response != null ? "result: " + response.toString() : "there is no stations within input radius");
    }

    private static void callGetCompanyById(RestTemplate restTemplate) {
        LOGGER.info("calling getCompanyById service");
        Map<String, String> params = new HashMap<>();
        params.put("id", "1");
        Company response = restTemplate.getForObject(
                URL + "{id}",
                Company.class,
                params);

        LOGGER.info(response != null ? "result: " + response.toString() : "no company found");
    }

    private static void callSaveCompany(RestTemplate restTemplate) {
        LOGGER.info("calling saveCompany service");
        Company request = Company.builder().name("K").parentCompanyId("3").build();
        Company response = restTemplate.postForObject(
                URL,
                request,
                Company.class);

        LOGGER.info(response != null ? "result: " + response.toString() : "no company found");
    }

    private static void callUpdateCompany(RestTemplate restTemplate) {
        LOGGER.info("calling updateCompany service");
        HttpHeaders headers = new HttpHeaders();
        headers.setIfMatch("1");
        Company company = Company.builder().id(4).name("K").parentCompanyId("3").build();
        HttpEntity<String> request = new HttpEntity(company, headers);

        Map<String, String> params = new HashMap<>();
        params.put("id", "1");

        ResponseEntity<Company> response = restTemplate.exchange(
                URL + "{id}",
                HttpMethod.PUT,
                request,
                Company.class,
                params);

        LOGGER.info("result: {}", response.toString());
    }

    private static void callRemoveCompany(RestTemplate restTemplate) {
        LOGGER.info("calling removeCompany service");
        Map<String, String> params = new HashMap<>();
        params.put("id", "5");
        ResponseEntity<List<Company>> response = restTemplate.exchange(
                URL + "{id}", HttpMethod.DELETE,
                null,
                new ParameterizedTypeReference<>() {
                },
                params);

        LOGGER.info("result: {}", response.toString());
    }

}
