package am.devolon.electric.station.client;

import am.devolon.electric.station.model.Station;
import am.devolon.electric.station.model.StationsRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * created by farhad (farhad.yousefi@outlook.com) on 3/24/2021 AD
 */
public class StationServicesClient {
    private static final Logger LOGGER = LogManager.getLogger(StationServicesClient.class);
    private static final String URL = "http://localhost:8989/electricStation/stations/";

    public static void main(String[] args) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        callGetStationById(restTemplate);
        callSaveStation(restTemplate);
        callUpdateStation(restTemplate);
        callRemoveStation(restTemplate);
    }

    private static void callGetStationById(RestTemplate restTemplate) {
        LOGGER.info("calling getStationById service");
        Map<String, String> params = new HashMap<>();
        params.put("id", "1");
        Station response = restTemplate.getForObject(
                URL + "{id}",
                Station.class,
                params);

        LOGGER.info(response != null ? "result: " + response.toString() : "no Station found");
    }

    private static void callSaveStation(RestTemplate restTemplate) {
        LOGGER.info("calling saveStation service");
        StationsRequest request  = new StationsRequest(2, 3.2282642020913, 35.2107776350628, "station10");

        Station response = restTemplate.postForObject(
                URL,
                request,
                Station.class);

        LOGGER.info(response != null ? "result: " + response.toString() : "no Station found");
    }

    private static void callUpdateStation(RestTemplate restTemplate) {
        LOGGER.info("calling updateStation service");
        HttpHeaders headers = new HttpHeaders();
        headers.setIfMatch("1");
        StationsRequest stationsRequest  = new StationsRequest(2, 3.2282642020913, 35.2107776350628, "station10");

        HttpEntity<String> request = new HttpEntity(stationsRequest, headers);

        Map<String, String> params = new HashMap<>();
        params.put("id", "1");

        ResponseEntity<Station> response = restTemplate.exchange(
                URL + "{id}",
                HttpMethod.PUT,
                request,
                Station.class,
                params);

        LOGGER.info("result: {}", response.toString());
    }

    private static void callRemoveStation(RestTemplate restTemplate) {
        LOGGER.info("calling removeStation service");
        Map<String, String> params = new HashMap<>();
        params.put("id", "1");
        ResponseEntity<List<Station>> response = restTemplate.exchange(
                URL + "{id}",
                HttpMethod.DELETE,
                null,
                new ParameterizedTypeReference<>() {},
                params);

        LOGGER.info("result: {}", response.toString());
    }

}
