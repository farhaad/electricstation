package am.devolon.electric.station.controller;

import am.devolon.electric.station.model.AllStationsRequest;
import am.devolon.electric.station.model.Company;
import am.devolon.electric.station.model.Station;
import am.devolon.electric.station.service.CompanyService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * created by farhad (farhad.yousefi@outlook.com) on 3/24/2021 AD
 */
@RestController
public class CompanyController {
    private static final Logger LOGGER = LogManager.getLogger(CompanyController.class);

    // for idempotency
    Map<Integer, Boolean> newAddedCompany = new HashMap<>();

    private final CompanyService companyService;

    @Autowired
    public CompanyController(CompanyService companyService) {
        this.companyService = companyService;
    }

    /**
     * Get all stations inside the input radius that are belong to the specific company
     *
     * @param id       of specific Company
     * @param criteria of the specific Company that maybe have some station(s) inside the given point (longitude,latitude) to get
     * @return ResponseEntity with the all found stations
     * or NOT_FOUND if no station found
     */
    @PostMapping("/companies/{id}/stations")
    public ResponseEntity<List<Station>> getAllStationsInsideRadius(@PathVariable Integer id, @RequestBody AllStationsRequest criteria) {
        try {
            criteria.setCompanyId(id);
            Optional<List<Station>> optional = companyService.findAllStationsWithinRadius(criteria);
            if (optional.isPresent()) {
                return ResponseEntity
                        .ok()
                        .body(optional.get());
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    /**
     * Get the Company with specified ID
     *
     * @param id ID of the Company to get
     * @return ResponseEntity with the found Comapny
     * or NOT_FOUND if no Company found
     */
    @GetMapping("companies/{id}")
    public ResponseEntity<Company> getCompanyById(@PathVariable Integer id) {
        Optional<Company> optional = companyService.findCompanyById(id);
        if (optional.isPresent()) {
            Company company = optional.get();
            try {
                return ResponseEntity
                        .ok()
                        .eTag(Integer.toString(company.getVersion()))
                        .location(new URI("/companies/" + company.getId()))
                        .body(company);
            } catch (URISyntaxException e) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
            }
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    /**
     * Saves a new Company
     *
     * @param company Company to save
     * @return ResponseEntity with the saved Company
     */
    @PostMapping("/companies")
    public ResponseEntity<?> saveCompany(@RequestBody Company company) {
        LOGGER.info("Adding new Company with name:{}", company.getName());

        //semi-idempotency
        if (newAddedCompany.containsKey(company.hashCode())) {
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        } else
            newAddedCompany.put(company.hashCode(), true);

        Company newCompany = companyService.addCompany(company);
        try {
            return ResponseEntity
                    .created(new URI("/companies/" + newCompany.getId()))
                    .eTag(Integer.toString(newCompany.getVersion()))
                    .body(newCompany);
        } catch (URISyntaxException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    /**
     * Update an existing Company
     *
     * @param criteria Company to update
     * @param ifMatch  eTag version of the Company to update
     * @return ResponseEntity with the updated Company
     * or CONFLICT if eTag versions do not match
     */
    @PutMapping("/companies/{id}")
    public ResponseEntity<Company> updateCompany(@PathVariable Integer id,
                                           @RequestBody Company criteria,
                                           @RequestHeader("If-Match") Integer ifMatch) {
        criteria.setId(id);
        Optional<Company> optional = companyService.findCompanyById(id);

        if (optional.isEmpty()) {
            LOGGER.error("Company with id {} not be found!", criteria.getId());
            return ResponseEntity.notFound().build();
        } else {
            Company existingCompany = optional.get();
            if (!existingCompany.getVersion().equals(ifMatch)) {
                return ResponseEntity.status(HttpStatus.CONFLICT).build();
            } else {
                LOGGER.info("Updating Company with name:{}", existingCompany.getName());
                try {
                    Optional<Company> savedOptional = companyService.updateCompany(existingCompany, criteria);
                    if (savedOptional.isEmpty()) {
                        LOGGER.error("parent id {} not be found!", criteria.getParentCompanyId());
                        return ResponseEntity.badRequest().build();
                    }
                    Company c = savedOptional.get();
                    return ResponseEntity
                            .ok()
                            .eTag(String.valueOf(c.getVersion()))
                            .location(new URI("/companies/" + c.getId()))
                            .body(c);
                } catch (URISyntaxException e) {
                    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
                }
            }
        }
    }

    /**
     * remove an existing Company and all of it children
     *
     * @param id Company id to delete
     * @return ResponseEntity with HTTP status
     */
    @DeleteMapping("/companies/{id}")
    public ResponseEntity<List<Company>> removeCompany(@PathVariable Integer id) {
        LOGGER.info("Deleting Company with id:{}", id);

        Optional<Company> optional = companyService.findCompanyById(id);
        if (optional.isPresent()) {
            List<Company> removedCompanies = companyService.removeCompany(optional.get());
            return ResponseEntity
                    .ok()
                    .body(removedCompanies);
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
