package am.devolon.electric.station.controller;

import am.devolon.electric.station.model.Station;
import am.devolon.electric.station.model.StationsRequest;
import am.devolon.electric.station.service.StationService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * created by farhad (farhad.yousefi@outlook.com) on 3/24/2021 AD
 */
@RestController
public class StationController {
    private static final Logger LOGGER = LogManager.getLogger(StationController.class);
    Map<Integer, Boolean> newAddedStations = new HashMap<>();

    private final StationService stationService;

    @Autowired
    public StationController(StationService stationService) {
        this.stationService = stationService;
    }

    /**
     * Get the Station with specified ID
     *
     * @param id ID of the Station to get
     * @return ResponseEntity with the found station
     * or NOT_FOUND if no Station found
     */
    @GetMapping("stations/{id}")
    public ResponseEntity<?> getStation(@PathVariable Integer id) {
        Optional<Station> optional = stationService.findStationById(id);
        if (optional.isPresent()) {
            Station station = optional.get();
            try {
                return ResponseEntity
                        .ok()
                        .eTag(Integer.toString(station.getVersion()))
                        .location(new URI("/stations/" + station.getId()))
                        .body(station);
            } catch (URISyntaxException e) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
            }
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    /**
     * Saves a new Station
     *
     * @param criteria Station to save
     * @return ResponseEntity with the saved Station
     */
    @PostMapping("/stations")
    public ResponseEntity<?> saveCompany(@RequestBody StationsRequest criteria) {
        LOGGER.info("Adding new Station with name:{}", criteria.getName());

        if (newAddedStations.containsKey(criteria.hashCode())) {
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        } else
            newAddedStations.put(criteria.hashCode(), true);

        Optional<Station> optional = stationService.addStation(criteria);
        if (optional.isEmpty())
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();

        try {
            Station newStation = optional.get();
            return ResponseEntity
                    .created(new URI("/stations/" + newStation.getId()))
                    .eTag(Integer.toString(newStation.getVersion()))
                    .body(newStation);
        } catch (URISyntaxException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    /**
     * Update an existing stations
     *
     * @param criteria stations to update
     * @param ifMatch  eTag version of the stations to update
     * @return ResponseEntity with the updated stations
     * or CONFLICT if eTag versions do not match
     */
    @PutMapping("/stations/{id}")
    public ResponseEntity<?> updateStation(@PathVariable @NotBlank Integer id,
                                           @Valid @RequestBody StationsRequest criteria,
                                           @RequestHeader("If-Match") Integer ifMatch) {

        Optional<Station> optional = stationService.findStationById(id);

        if (optional.isEmpty()) {
            LOGGER.error("Station with id {} not be found!", id);
            return ResponseEntity.notFound().build();
        } else {
            Station existingStation = optional.get();
            if (!existingStation.getVersion().equals(ifMatch)) {
                return ResponseEntity.status(HttpStatus.CONFLICT).build();
            } else {
                LOGGER.info("Updating Station with name:{}", existingStation.getName());
                try {
                    Optional<Station> savedOptional = stationService.updateStation(existingStation, criteria);
                    if (savedOptional.isEmpty()) {
                        LOGGER.error("company id {} not be found!", criteria.getCompanyId());
                        return ResponseEntity.badRequest().build();
                    }

                    Station station = savedOptional.get();
                    return ResponseEntity
                            .ok()
                            .eTag(Integer.toString(station.getVersion()))
                            .location(new URI("/stations/" + station.getId()))
                            .body(station);
                } catch (URISyntaxException e) {
                    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
                }
            }
        }
    }

    /**
     * remove an existing Station
     *
     * @param id Station id to delete
     * @return ResponseEntity with HTTP status
     */
    @DeleteMapping("/stations/{id}")
    public ResponseEntity<?> removeStation(@PathVariable @NotBlank Integer id) {
        LOGGER.info("Deleting Station with id:{}", id);

        Optional<Station> optional = stationService.findStationById(id);
        if (optional.isPresent()) {
            stationService.removeStation(optional.get());
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

}
