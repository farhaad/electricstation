package am.devolon.electric.station.model;

import lombok.Data;

/**
 * created by farhad (farhad.yousefi@outlook.com) on 3/23/2021 AD
 */
@Data
public class AllStationsRequest {
    private Integer companyId;
    private Double longitude;
    private Double latitude;
    private Integer distance;

    public AllStationsRequest(Double longitude, Double latitude, Integer distance) {
        this.longitude = longitude;
        this.latitude = latitude;
        this.distance = distance;
    }
}
