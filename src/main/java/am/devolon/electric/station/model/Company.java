package am.devolon.electric.station.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

/**
 * created by farhad (farhad.yousefi@outlook.com) on 3/23/2021 AD
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Company {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    @Column(name = "parent_company_id", columnDefinition = "ltree")
    private String parentCompanyId;

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "company")
    private Set<Station> stations;

    @Version
    //@JsonIgnore
    private Integer version;

    public Company(Integer id, String name, String parentCompanyId,Integer version) {
        if (id != null)
            this.id = id;
        this.name = name;
        this.parentCompanyId = parentCompanyId;
        if(version!=null)
            this.version = version;
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + name.hashCode();
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (version != null ? version.hashCode() : 0);
        result = 31 * result + parentCompanyId.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Company other = (Company) obj;
        if (!this.parentCompanyId.equals(other.parentCompanyId)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }

        if (!Objects.equals(this.version, other.version)) {
            return false;
        }
        return Objects.equals(this.id, other.id);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Company{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", parentCompanyId=").append(parentCompanyId);
        sb.append('}');
        return sb.toString();
    }
}
