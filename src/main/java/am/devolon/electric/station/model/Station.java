package am.devolon.electric.station.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Objects;

/**
 * created by farhad (farhad.yousefi@outlook.com) on 3/23/2021 AD
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Station {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private Double latitude;
    private Double longitude;

    @Version
    @JsonIgnore
    private Integer version;

    @ManyToOne
    @JoinColumn(name = "company_id", insertable = true, updatable = true, nullable = false)
    @JsonIgnore
    private Company company;

    public Station(Integer id, String name, Double latitude, Double longitude, Integer version) {
        this.id = id;
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.version = version;
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + name.hashCode();
        result = 31 * result + id.hashCode();
        result = 31 * result + longitude.hashCode();
        result = 31 * result + latitude.hashCode();
        result = 31 * result + company.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Station other = (Station) obj;
        if (this.company.getId().equals(other.company.getId())) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.latitude, other.latitude)) {
            return false;
        }
        if (!Objects.equals(this.longitude, other.longitude)) {
            return false;
        }
        return Objects.equals(this.id, other.id);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Station{");
        if (id != null) {
            sb.append(" id=").append(id);
        }
        sb.append(", name='").append(name).append('\'');
        sb.append(", point=(").append(latitude).append(",").append(longitude).append(")");
        if (company != null) {
            sb.append(", companyId=").append(company.getId());
        }
        sb.append('}');
        return sb.toString();
    }
}
