package am.devolon.electric.station.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

/**
 * created by farhad (farhad.yousefi@outlook.com) on 3/23/2021 AD
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StationsRequest {
    private Integer companyId;
    private Double longitude;
    private Double latitude;

    @NotBlank(message = "Name is mandatory")
    private String name;
}
