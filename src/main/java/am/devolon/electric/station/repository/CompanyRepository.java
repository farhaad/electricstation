package am.devolon.electric.station.repository;

import am.devolon.electric.station.model.Company;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * created by farhad (farhad.yousefi@outlook.com) on 3/24/2021 AD
 */

@Repository
public interface CompanyRepository extends CrudRepository<Company, Integer> {
    @Query(value = "insert into DEVOLON.company (name,PARENT_COMPANY_ID,version) " +
            "values (:name,CAST(:path AS ltree),:version) RETURNING id", nativeQuery = true)
    Integer saveCompany(@Param("name") String name,
                        @Param("path") String ltree,
                        @Param("version") Integer version
    );

    @Query(value = "update DEVOLON.company set name = :name, PARENT_COMPANY_ID = CAST(:path AS ltree), version =:version " +
            "where id=:id and version=:version-1 RETURNING id ", nativeQuery = true)
    Integer updateCompany(@Param("name") String name,
                          @Param("path") String ltree,
                          @Param("version") Integer version,
                          @Param("id") Integer id
    );

    @Query(value = "SELECT * FROM DEVOLON.company where parent_company_id ~ lquery(:path)", nativeQuery = true)
    List<Company> findByParentCompanyId(@Param("path") String ltree);

}