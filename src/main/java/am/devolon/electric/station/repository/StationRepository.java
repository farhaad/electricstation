package am.devolon.electric.station.repository;

import am.devolon.electric.station.model.Station;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * created by farhad (farhad.yousefi@outlook.com) on 3/24/2021 AD
 */

@Repository
public interface StationRepository extends JpaRepository<Station, Integer> {
    @Query(
            value = "select * from (" +
                    "                  SELECT  s.*, " +
                    "                          (6371 * acos( cos( radians(:latitude) ) * " +
                    "                                        cos( radians( latitude ) ) * " +
                    "                                        cos( radians( longitude ) - radians(:longitude) ) + " +
                    "                                        sin( radians(:latitude) ) * sin( radians( latitude ) ) ) " +
                    "                              ) as distance " +
                    "                  FROM DEVOLON.station s inner join DEVOLON.company c on c.id = s.company_id " +
                    "                  where c.parent_company_id ~ lquery(:path) or c.id = :id " +
                    "              ) al " +
                    "where distance <= :distance " +
                    "ORDER BY distance ",
            nativeQuery = true)
    List<Station> findAllInsideRadius(
            @Param("id") Integer companyId,
            @Param("path") String parentCompany,
            @Param("longitude") Double longitude,
            @Param("latitude") Double latitude,
            @Param("distance") Integer distance
    );

    @Query(value = "update DEVOLON.station set name = :name, latitude = :latitude,longitude=:longitude, company_id=:companyId, " +
            "version =:version where id=:id and version=:version-1 RETURNING id", nativeQuery = true)
    Integer updateStation(@Param("name") String name,
                          @Param("latitude") Double latitude,
                          @Param("longitude") Double longitude,
                          @Param("companyId") Integer companyId,
                          @Param("version") Integer version,
                          @Param("id") Integer id
    );

    @Query(value = "insert into DEVOLON.station (name,latitude,longitude,company_id,version) " +
            "values( :name,:latitude,:longitude,:companyId,:version ) RETURNING id", nativeQuery = true)
    Integer addStation(@Param("name") String name,
                       @Param("latitude") Double latitude,
                       @Param("longitude") Double longitude,
                       @Param("companyId") Integer companyId,
                       @Param("version") Integer version
    );

}
