package am.devolon.electric.station.service;

import am.devolon.electric.station.model.AllStationsRequest;
import am.devolon.electric.station.model.Company;
import am.devolon.electric.station.model.Station;

import java.util.List;
import java.util.Optional;

/**
 * created by farhad (farhad.yousefi@outlook.com) on 3/24/2021 AD
 */
public interface CompanyService {
    Optional<Company> findCompanyById(Integer id);

    Company addCompany(Company company);

    Optional<Company> updateCompany(Company existingCompany,Company criteria);

    List<Company> removeCompany(Company company);

    Optional<List<Station>> findAllStationsWithinRadius(AllStationsRequest criteria);
}
