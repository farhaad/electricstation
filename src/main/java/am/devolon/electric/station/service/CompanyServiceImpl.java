package am.devolon.electric.station.service;

import am.devolon.electric.station.model.AllStationsRequest;
import am.devolon.electric.station.model.Company;
import am.devolon.electric.station.model.Station;
import am.devolon.electric.station.repository.CompanyRepository;
import am.devolon.electric.station.repository.StationRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * created by farhad (farhad.yousefi@outlook.com) on 3/24/2021 AD
 */

@Service
public class CompanyServiceImpl implements CompanyService {
    private static final Logger LOGGER = LogManager.getLogger(CompanyServiceImpl.class);
    private final CompanyRepository companyRepository;
    private final StationRepository stationRepository;

    @Autowired
    public CompanyServiceImpl(CompanyRepository companyRepository, StationRepository stationRepository) {
        this.companyRepository = companyRepository;
        this.stationRepository = stationRepository;
    }

    @Override
    public Optional<List<Station>> findAllStationsWithinRadius(AllStationsRequest criteria) {
        LOGGER.info("finding all station inside the input radius that are belong to the company {}", criteria.getCompanyId());
        Optional<Company> companyOptional = findCompanyById(criteria.getCompanyId());

        if (companyOptional.isEmpty())
            return Optional.empty();

        List<Station> stations = stationRepository.findAllInsideRadius(
                criteria.getCompanyId(),
                "*." + criteria.getCompanyId() + ".*",
                criteria.getLongitude(), criteria.getLatitude(),
                criteria.getDistance());

        return Optional.of(stations);
    }


    @Override
    public Optional<Company> findCompanyById(Integer id) {
        LOGGER.info("finding company by id {}", id);
        Optional<Company> optional = companyRepository.findById(id);
        if (optional.isPresent()) {
            Company company = optional.get();
            //null means the company is root, and just one parent means there is no ancestor
            removeAncestor(company);
            optional = Optional.of(company);
        }
        return optional;
    }

    //remove all ancestor except direct parent's company in service response
    private void removeAncestor(Company company) {
        if (company.getParentCompanyId() != null && company.getParentCompanyId().length() > 1) {
            String[] ancestor = company.getParentCompanyId().split("\\.");
            company.setParentCompanyId(ancestor[ancestor.length - 1]);
        }
    }

    @Override
    @Transactional
    public Company addCompany(Company company) {
        company.setVersion(1);
        //step 1: find ltree of input company
        Optional<Company> optional = companyRepository.findById(Integer.valueOf(company.getParentCompanyId()));
        if (optional.isPresent()) {
            Company parent = optional.get();
            company.setParentCompanyId(
                    parent.getParentCompanyId() != null ? parent.getParentCompanyId() + "." + parent.getId() : String.valueOf(parent.getId())
            );
        } else {
            LOGGER.info("root company ");
        }

        //step 2: save the company
        company.setId(companyRepository.saveCompany(company.getName(), company.getParentCompanyId(), company.getVersion()));
        removeAncestor(company);
        return company;
    }

    @Override
    public Optional<Company> updateCompany(Company existingCompany, Company criteria) {
        LOGGER.info("Updating company with id:{}", criteria.getId());

        if (criteria.getName() != null && !criteria.getName().isBlank())
            existingCompany.setName(criteria.getName());

        if (criteria.getParentCompanyId() != null && !criteria.getParentCompanyId().isBlank()) {
            Optional<Company> parentOptional = companyRepository.findById(Integer.valueOf(criteria.getParentCompanyId()));
            if (parentOptional.isEmpty()) {
                LOGGER.error("parent company with id {} could not be found!", criteria.getParentCompanyId());
                return Optional.empty();
            } else {
                Company parent = parentOptional.get();
                existingCompany.setParentCompanyId(
                        parent.getParentCompanyId() == null ?
                                String.valueOf(parent.getId()) : parent.getParentCompanyId() + "." + parent.getId());
            }
        } else {//root company
            existingCompany.setParentCompanyId(null);
        }

        existingCompany.setVersion(existingCompany.getVersion() + 1);

        Integer id = companyRepository.updateCompany(
                existingCompany.getName(), existingCompany.getParentCompanyId(), existingCompany.getVersion(), existingCompany.getId());

        LOGGER.info("record related to id {} has been updated", id);
        removeAncestor(existingCompany);
        return Optional.of(existingCompany);
    }

    @Override
    public List<Company> removeCompany(Company company) {
        LOGGER.info("deleting company with id:{}", company.getId());
        List<Company> removedCompany = new ArrayList<>();

        //cascading:
        // remove all children of company that has input company id in their parentCompanyId
        // and all of their station will be removed
        // all of station belong to company will be remove because of cascading
        List<Company> companies = companyRepository.findByParentCompanyId("*." + company.getId() + ".*"); //all children
        companies.add(company);//now companies contains input company and its children
        companies.forEach(e -> {
            companyRepository.delete(e);
            removeAncestor(e);
            removedCompany.add(e);
        });
        return removedCompany;
    }

}
