package am.devolon.electric.station.service;

import am.devolon.electric.station.model.Station;
import am.devolon.electric.station.model.StationsRequest;

import java.util.Optional;

/**
 * created by farhad (farhad.yousefi@outlook.com) on 3/24/2021 AD
 */
public interface StationService {

    Optional<Station> findStationById(Integer id);

    Optional<Station> addStation(StationsRequest station);

    Optional<Station> updateStation(Station existingStation, StationsRequest criteria);

    void removeStation(Station station);
}
