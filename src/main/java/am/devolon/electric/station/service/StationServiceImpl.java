package am.devolon.electric.station.service;

import am.devolon.electric.station.model.Company;
import am.devolon.electric.station.model.Station;
import am.devolon.electric.station.model.StationsRequest;
import am.devolon.electric.station.repository.StationRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * created by farhad (farhad.yousefi@outlook.com) on 3/24/2021 AD
 */
@Service
public class StationServiceImpl implements StationService {
    private static final Logger LOGGER = LogManager.getLogger(StationServiceImpl.class);

    private final StationRepository stationRepository;
    private final CompanyService companyService;

    @Autowired
    public StationServiceImpl(StationRepository stationRepository, CompanyService companyService) {
        this.stationRepository = stationRepository;
        this.companyService = companyService;
    }

    @Override
    public Optional<Station> findStationById(Integer id) {
        LOGGER.info("finding station by id {}", id);
        return stationRepository.findById(id);
    }

    @Override
    public Optional<Station> addStation(StationsRequest criteria) {
        LOGGER.info("adding station {} that belong to {} company", criteria.getName(), criteria.getCompanyId());

        Station station = new Station();
        station.setName(criteria.getName());
        station.setLongitude(criteria.getLongitude());
        station.setLatitude(criteria.getLatitude());
        station.setVersion(1);

        if (criteria.getCompanyId() != null) {
            Optional<Company> companyOptional = companyService.findCompanyById(criteria.getCompanyId());
            if (companyOptional.isPresent())
                station.setCompany(companyOptional.get());
            else
                return Optional.empty();
        }
        Integer id = stationRepository.addStation(
                station.getName(), station.getLatitude(), station.getLongitude(), station.getCompany().getId(), station.getVersion()
        );
        station.setId(id);

        return Optional.of(station);
    }

    @Override
    public Optional<Station> updateStation(Station existingStation, StationsRequest criteria) {
        LOGGER.info("Updating station with id:{}", existingStation.getId());

        if (criteria.getCompanyId() != null) {
            //check company that station is belong to, is exist or not
            Optional<Company> companyOptional = companyService.findCompanyById(criteria.getCompanyId());
            if (companyOptional.isEmpty())
                return Optional.empty();

            existingStation.setCompany(companyOptional.get());
        }
        if (criteria.getLatitude() != null)
            existingStation.setLatitude(criteria.getLatitude());

        if (criteria.getLongitude() != null)
            existingStation.setLongitude(criteria.getLongitude());

        if (criteria.getName() != null && !criteria.getName().isBlank())
            existingStation.setName(criteria.getName());

        existingStation.setVersion(existingStation.getVersion() + 1);
        Integer id = stationRepository.updateStation(existingStation.getName(),
                existingStation.getLatitude(),
                existingStation.getLongitude(),
                existingStation.getCompany().getId(),
                existingStation.getVersion(),
                existingStation.getId());

        return Optional.of(existingStation);
    }

    @Override
    public void removeStation(Station station) {
        LOGGER.info("deleting station with id:{}", station.getId());
        stationRepository.delete(station);
    }

}
