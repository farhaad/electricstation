-- Insert values
insert into DEVOLON.company (name, parent_company_id,version) values ('A', null,1);
insert into DEVOLON.company (name, parent_company_id,version) values ('B', '1',1);
insert into DEVOLON.company (name, parent_company_id,version) values ('C', '1',1);
insert into DEVOLON.company (name, parent_company_id,version) values ('D', '1.3',1);
insert into DEVOLON.company (name, parent_company_id,version) values ('E', '1.3',1);
insert into DEVOLON.company (name, parent_company_id,version) values ('F', '3.4',1);
insert into DEVOLON.company (name, parent_company_id,version) values ('G', '1.2',1);

insert into DEVOLON.Station(name,latitude,longitude,company_id,version) values ('station1',11.2280839022311,35.2108630836368,1,1);
insert into DEVOLON.Station(name,latitude,longitude,company_id,version) values ('station2',12.2285534862982,34.2107802226056,1,1);
insert into DEVOLON.Station(name,latitude,longitude,company_id,version) values ('station3',13.2282642020913,35.2107776350628,1,1);
insert into DEVOLON.Station(name,latitude,longitude,company_id,version) values ('station4',14.2282413282919,36.2107668435967,2,1);
insert into DEVOLON.Station(name,latitude,longitude,company_id,version) values ('station5',14.2282197996767,35.2107650450756,3,1);
insert into DEVOLON.Station(name,latitude,longitude,company_id,version) values ('station6',14.2282130720501,37.2107623473294,4,1);
insert into DEVOLON.Station(name,latitude,longitude,company_id,version) values ('station7',11.2282009623452,38.2107632469164,4,1);
insert into DEVOLON.Station(name,latitude,longitude,company_id,version) values ('station8',11.2281861617643,38.2107731412360,5,1);
