package am.devolon.electric.station.controller;

import am.devolon.electric.station.model.AllStationsRequest;
import am.devolon.electric.station.model.Company;
import am.devolon.electric.station.model.Station;
import am.devolon.electric.station.service.CompanyService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * created by farhad (farhad.yousefi@outlook.com) on 3/23/2021 AD
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext
@AutoConfigureMockMvc
public class CompanyControllerTest {
    @MockBean
    private CompanyService companyService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    @DisplayName("Test get all stations inside the input radius that are belong to the specific company - GET /companies/{id}/stations")
    public void testGetAllStations() throws Exception {
        AllStationsRequest mockRequest = new AllStationsRequest(34.210, 10.3, 300);
        mockRequest.setCompanyId(1);

        List<Station> mockStations = new ArrayList<>();
        mockStations.add(new Station(1, "station1", 11.2280839022311, 35.2108630836368, 1));
        mockStations.add(new Station(2, "station2", 12.2285534862982, 34.2107802226056, 1));
        Optional<List<Station>> mockOptional = Optional.of(mockStations);

        doReturn(mockOptional).when(companyService).findAllStationsWithinRadius(ArgumentMatchers.any());

        mockMvc.perform(post("/companies/{id}/stations", 1)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(new ObjectMapper().writeValueAsString(mockRequest)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$[0].name", is("station1")))
                .andExpect(jsonPath("$[1].name", is("station2")));
    }

    @Test
    @DisplayName("Test Company found - GET /companies/6")
    public void testGetCompanyById() throws Exception {
        Optional<Company> mockCompany = Optional.of(new Company(6, "F", "4", 1));

        doReturn(mockCompany).when(companyService).findCompanyById(6);

        mockMvc.perform(MockMvcRequestBuilders.get("/companies/{id}", 6))
                // Validate 200 OK and JSON response type received
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))

                // Validate response headers
                .andExpect(header().string(HttpHeaders.ETAG, "\"1\""))
                .andExpect(header().string(HttpHeaders.LOCATION, "/companies/6"))

                // Validate response body
                .andExpect(jsonPath("$.id", is(6)))
                .andExpect(jsonPath("$.name", is("F")))
                .andExpect(jsonPath("$.parentCompanyId", is("4")));
    }

    @Test
    @DisplayName("Test adding new company - POST /companies")
    public void testSaveCompany() throws Exception {
        Company newCompany = Company.builder().name("K").parentCompanyId("3").version(1).build();
        Company mockCompany = new Company(10, "K", "3", 1);

        doReturn(mockCompany).when(companyService).addCompany(ArgumentMatchers.any());

        mockMvc.perform(post("/companies")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(new ObjectMapper().writeValueAsString(newCompany)))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(header().string(HttpHeaders.ETAG, "\"1\""))
                .andExpect(header().string(HttpHeaders.LOCATION, "/companies/10"))
                .andExpect(jsonPath("$.id", is(10)))
                .andExpect(jsonPath("$.name", is("K")))
                .andExpect(jsonPath("$.parentCompanyId", is("3")));
    }

    @Test
    @DisplayName("Update an existing company with success - PUT /companies/2")
    public void testUpdateCompany() throws Exception {
        Company companyToUpdate = new Company(2, "new B", "3", 1);

        Optional<Company> mockCompany = Optional.of(new Company(2, "B", "2", 1));

        Optional<Company> mockSavedCompany = Optional.of(new Company(2, "new B", "3", 2));

        doReturn(mockCompany).when(companyService).findCompanyById(2);
        doReturn(mockSavedCompany).when(companyService).updateCompany(ArgumentMatchers.any(), ArgumentMatchers.any());

        mockMvc.perform(put("/companies/{id}", 2)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .header(HttpHeaders.IF_MATCH, 1)
                .content(new ObjectMapper().writeValueAsString(companyToUpdate)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(header().string(HttpHeaders.ETAG, "\"2\""))
                .andExpect(header().string(HttpHeaders.LOCATION, "/companies/2"))
                .andExpect(jsonPath("$.id", is(2)))
                .andExpect(jsonPath("$.name", is("new B")))
                .andExpect(jsonPath("$.parentCompanyId", is("3")));
    }

    @Test
    @DisplayName("Version mismatch while updating existing company - PUT /companies/2")
    public void testVersionMismatchWhileUpdating() throws Exception {
        Company companyToUpdate = new Company(2, "new B", "3", 1);

        Optional<Company> mockCompany = Optional.of(new Company(2, "new B", "3", 2));

        doReturn(mockCompany).when(companyService).findCompanyById(2);

        mockMvc.perform(put("/companies/{id}", 2)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .header(HttpHeaders.IF_MATCH, 1)
                .content(new ObjectMapper().writeValueAsString(companyToUpdate)))
                .andExpect(status().isConflict());
    }

    @Test
    @DisplayName("Version mismatch while updating existing company - PUT /companies/2")
    public void testCompanyNotFoundWhileUpdating() throws Exception {
        Company companyToUpdate = new Company(2, "new B", "3", 1);
        doReturn(Optional.empty()).when(companyService).findCompanyById(2);

        mockMvc.perform(put("/companies/{id}", 2)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .header(HttpHeaders.IF_MATCH, 1)
                .content(new ObjectMapper().writeValueAsString(companyToUpdate)))
                .andExpect(status().isNotFound());
    }

    @Test
    @DisplayName("Delete a company successfully - DELETE /companies/2")
    public void testCompanyDeletedSuccessfully() throws Exception {
        Optional<Company> mockCompany = Optional.of(new Company(2, "B", "2", 1));
        doReturn(mockCompany).when(companyService).findCompanyById(2);

        List<Company> mockRemovedCompany = new ArrayList<>();
        mockRemovedCompany.add(new Company(2, "B", "1", 1));
        mockRemovedCompany.add(new Company(4, "D", "2", 1));

        doReturn(mockRemovedCompany).when(companyService).removeCompany(ArgumentMatchers.any());

        // Perform DELETE request
        mockMvc.perform(delete("/companies/{id}", 2))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$[?(@.id)]", hasSize(2)))
                .andExpect(jsonPath("$[*].name", containsInAnyOrder("B", "D")))
                .andExpect(jsonPath("$[*].parentCompanyId", containsInAnyOrder("1", "2")));
    }

    @Test
    @DisplayName("Fail to delete an non-existing company - DELETE /companies/10")
    public void testFailureToDeleteNonExistingCompany() throws Exception {
        doReturn(null).when(companyService).findCompanyById(10);

        mockMvc.perform(delete("/companies/{id}", 1))
                .andExpect(status().isNotFound());
    }

}
