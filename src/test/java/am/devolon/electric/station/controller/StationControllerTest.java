package am.devolon.electric.station.controller;

import am.devolon.electric.station.model.Station;
import am.devolon.electric.station.model.StationsRequest;
import am.devolon.electric.station.service.StationService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * created by farhad (farhad.yousefi@outlook.com) on 3/23/2021 AD
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext
@AutoConfigureMockMvc
public class StationControllerTest {
    @MockBean
    private StationService stationService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    @DisplayName("Test station found - GET /stations/3")
    public void testGetStationById() throws Exception {
        Optional<Station> mockStation = Optional.of(new Station(3, "station3", 3.2282642020913, 35.2107776350628, 1));

        doReturn(mockStation).when(stationService).findStationById(anyInt());

        mockMvc.perform(MockMvcRequestBuilders.get("/stations/{id}", 3))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(header().string(HttpHeaders.ETAG, "\"1\""))
                .andExpect(header().string(HttpHeaders.LOCATION, "/stations/3"))
                .andExpect(jsonPath("$.id", is(3)))
                .andExpect(jsonPath("$.name", is("station3")))
                .andExpect(jsonPath("$.latitude", is(3.2282642020913)))
                .andExpect(jsonPath("$.longitude", is(35.2107776350628)));
    }

    @Test
    @DisplayName("Test adding new Station - POST /stations")
    public void testSaveStation() throws Exception {
        StationsRequest stationsRequest = new StationsRequest(2, 3.2282642020913, 35.2107776350628, "station10");
        Station mockStation = new Station(10, "station10", 3.2282642020913, 35.2107776350628, 1);

        doReturn(Optional.of(mockStation)).when(stationService).addStation(ArgumentMatchers.any());

        mockMvc.perform(post("/stations")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(new ObjectMapper().writeValueAsString(stationsRequest)))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(header().string(HttpHeaders.ETAG, "\"1\""))
                .andExpect(header().string(HttpHeaders.LOCATION, "/stations/10"))
                .andExpect(jsonPath("$.id", is(10)))
                .andExpect(jsonPath("$.name", is("station10")))
                .andExpect(jsonPath("$.latitude", is(3.2282642020913)))
                .andExpect(jsonPath("$.longitude", is(35.2107776350628)));
    }

    @Test
    @DisplayName("Update an existing station with success - PUT /stations/2")
    public void testUpdateStation() throws Exception {
        StationsRequest mockCriteria = new StationsRequest(
                2, 13.2282642020913, 55.2107776350628, "update station10");
        Optional<Station> mockExistingStation = Optional.of(
                new Station(2, "station10", 3.2282642020913, 35.2107776350628, 1));

        Optional<Station> mockSavedStation = Optional.of(new Station(
                2, "update station10", 13.4282642020913, 55.2107776350628, 2));

        doReturn(mockExistingStation).when(stationService).findStationById(2);
        doReturn(mockSavedStation).when(stationService).updateStation(mockExistingStation.get(), mockCriteria);

        mockMvc.perform(put("/stations/{id}", 2)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .header(HttpHeaders.IF_MATCH, 1)
                .content(new ObjectMapper().writeValueAsString(mockCriteria)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(header().string(HttpHeaders.ETAG, "\"2\""))
                .andExpect(header().string(HttpHeaders.LOCATION, "/stations/2"))
                .andExpect(jsonPath("$.name", is("update station10")))
                .andExpect(jsonPath("$.latitude", is(13.4282642020913)))
                .andExpect(jsonPath("$.longitude", is(55.2107776350628)));
    }

    @Test
    @DisplayName("Delete a station successfully - DELETE /stations/2")
    public void testStationDeletedSuccessfully() throws Exception {
        Optional<Station> mockStation = Optional.of(
                new Station(2, "station10", 3.2282642020913, 35.2107776350628, 1));

        doReturn(mockStation).when(stationService).findStationById(2);
        doNothing().when(stationService).removeStation(ArgumentMatchers.any());

        mockMvc.perform(delete("/stations/{id}", 2))
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("Fail to delete an non-existing staiton - DELETE /stations/100")
    public void testFailureToDeleteNonExistingCompany() throws Exception {
        doReturn(null).when(stationService).findStationById(100);

        mockMvc.perform(delete("/stations/{id}", 1))
                .andExpect(status().isNotFound());
    }
}
