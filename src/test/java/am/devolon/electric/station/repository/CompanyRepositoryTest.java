package am.devolon.electric.station.repository;

import am.devolon.electric.station.model.Company;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.event.annotation.AfterTestClass;
import org.springframework.test.context.event.annotation.BeforeTestClass;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

/**
 * created by farhad (farhad.yousefi@outlook.com) on 3/23/2021 AD
 */

@ExtendWith({SpringExtension.class})
@SpringBootTest
public class CompanyRepositoryTest {

    @Autowired
    private CompanyRepository companyRepository;

    private static boolean isInitialized;
    private static File DATA_JSON = Paths.get("src", "test", "resources", "companies.json").toFile();

    @BeforeTestClass
    public void setup() throws IOException {
        if(!isInitialized){
            // Deserialize company from JSON file to Company array
            Company[] companies = new ObjectMapper().readValue(DATA_JSON, Company[].class);

            // Save each Company to database
            Arrays.stream(companies).forEach(e -> {
                companyRepository.saveCompany(e.getName(), e.getParentCompanyId(), e.getVersion());
            });
            isInitialized = true;
        }
    }

    @AfterTestClass
    public void cleanup() {
        // Cleanup the database after each test
        companyRepository.deleteAll();
    }

    @Test
    @DisplayName("Test company found")
    public void testCompanyFound() {
        Optional<Company> retrievedCompany = companyRepository.findById(1);
        assertTrue(retrievedCompany.isPresent());
        assertEquals(retrievedCompany.get().getName(), "A");
    }

    @Test
    @DisplayName("Test company not found with non-existing id")
    public void testCompanyNotFoundForNonExistingId() {
        Optional<Company> retrievedCompany = companyRepository.findById(10000);
        assertTrue(retrievedCompany.isEmpty());
    }

    @Test
    @DisplayName("Test Company saved successfully")
    public void testCompanySavedSuccessfully() {
        Company newCompany = new Company(null, "K", "1.3", 1);

        Integer id = companyRepository.saveCompany(newCompany.getName(), newCompany.getParentCompanyId(), newCompany.getVersion());
        Assertions.assertNotNull(id, "company should be saved");
        Company insertedCompany = companyRepository.findById(id).get();
        Assertions.assertEquals(newCompany.getName(), insertedCompany.getName());
        Assertions.assertEquals(newCompany.getParentCompanyId(), insertedCompany.getParentCompanyId());
        Assertions.assertEquals(newCompany.getVersion(), insertedCompany.getVersion());

    }

    @Test
    @DisplayName("Test company updated successfully")
    public void testCompanyUpdatedSuccessfully() {
        Company companyToUpdate = new Company(4, "G", "1.2", 2);
        Integer id = companyRepository.updateCompany(
                companyToUpdate.getName(), companyToUpdate.getParentCompanyId(), companyToUpdate.getVersion(), companyToUpdate.getId());

        Optional<Company> optionalCompany = companyRepository.findById(id);
        if (optionalCompany.isPresent()) {
            Company updatedCompany = optionalCompany.get();
            Assertions.assertEquals(companyToUpdate.getName(), updatedCompany.getName());
            Assertions.assertEquals(companyToUpdate.getParentCompanyId(), updatedCompany.getParentCompanyId());
            Assertions.assertEquals(companyToUpdate.getVersion(), updatedCompany.getVersion());
        } else
            fail("for running test correctly please run scripts inside schema.sql to drop.define company table again, " +
                    "or use a correct id of company");
    }

    @Test
    @DisplayName("Test company deleted successfully")
    public void testCompanyDeletedSuccessfully() {
        Optional<Company> optionalCompany = companyRepository.findById(1);
        optionalCompany.ifPresent(company -> companyRepository.delete(company));

        assertTrue(companyRepository.findById(1).isEmpty());
        //Assertions.assertEquals(6L, companyRepository.count());
    }


}
