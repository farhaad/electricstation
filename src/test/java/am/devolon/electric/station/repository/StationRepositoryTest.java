package am.devolon.electric.station.repository;

import am.devolon.electric.station.model.Station;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.event.annotation.AfterTestClass;
import org.springframework.test.context.event.annotation.BeforeTestClass;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

/**
 * created by farhad (farhad.yousefi@outlook.com) on 3/23/2021 AD
 */

@ExtendWith({SpringExtension.class})
@SpringBootTest
public class StationRepositoryTest {

    @Autowired
    private StationRepository stationRepository;

    private static boolean isInitialized;
    private static File DATA_JSON = Paths.get("src", "test", "resources", "stations.json").toFile();

    @BeforeTestClass
    public void setup() throws IOException {
        if (!isInitialized) {
            Station[] stations = new ObjectMapper().readValue(DATA_JSON, Station[].class);
            Arrays.stream(stations).forEach(e -> {
                stationRepository.addStation(e.getName(), e.getLatitude(), e.getLongitude(), e.getCompany().getId(), e.getVersion());
            });
            isInitialized = true;
        }
    }

    @AfterTestClass
    public void cleanup() {
        stationRepository.deleteAll();
    }

    @Test
    @DisplayName("Test station found")
    public void testStationFound() {
        Optional<Station> retrievedStation = stationRepository.findById(1);
        assertTrue(retrievedStation.isPresent());
        assertEquals(retrievedStation.get().getName(), "station1");
    }

    @Test
    @DisplayName("Test station not found with non-existing id")
    public void testCompanyNotFoundForNonExistingId() {
        Optional<Station> retrievedStation = stationRepository.findById(10000);
        assertTrue(retrievedStation.isEmpty());
    }

    @Test
    @DisplayName("Test station saved successfully")
    public void testStationSavedSuccessfully() {
        Station mockStationToInsert = new Station(10, "station10", 31.2280839022311, 35.2108630836368, 2);

        Integer id = stationRepository.addStation(mockStationToInsert.getName(), mockStationToInsert.getLatitude(),
                mockStationToInsert.getLongitude(), 1, mockStationToInsert.getVersion());

        Assertions.assertNotNull(id, "station should be saved");
        Station insertedStation = stationRepository.findById(id).get();
        Assertions.assertEquals(mockStationToInsert.getName(), insertedStation.getName());
        Assertions.assertEquals(mockStationToInsert.getLatitude(), insertedStation.getLatitude());
        Assertions.assertEquals(mockStationToInsert.getVersion(), insertedStation.getVersion());

    }

    @Test
    @DisplayName("Test Station updated successfully")
    public void testStationUpdatedSuccessfully() {
        Station mockStationToUpdate = new Station(2, "station2", 31.2280839022311, 35.2108630836368, 2);
        Integer id = stationRepository.updateStation(mockStationToUpdate.getName(), mockStationToUpdate.getLatitude(),
                mockStationToUpdate.getLongitude(), 1, mockStationToUpdate.getVersion(), mockStationToUpdate.getId());

        Optional<Station> optionalStation = stationRepository.findById(id);
        if (optionalStation.isPresent()) {
            Station updatedStation = optionalStation.get();
            Assertions.assertEquals(mockStationToUpdate.getName(), updatedStation.getName());
            Assertions.assertEquals(mockStationToUpdate.getLatitude(), updatedStation.getLatitude());
            Assertions.assertEquals(mockStationToUpdate.getLongitude(), updatedStation.getLongitude());
            Assertions.assertEquals(mockStationToUpdate.getVersion(), updatedStation.getVersion());
        } else
            fail("for running test correctly please run scripts inside schema.sql to drop.define station table again, " +
                    "or use a correct id of station");
    }

    @Test
    @DisplayName("Test station deleted successfully")
    public void testStationDeletedSuccessfully() {
        Optional<Station> optionalStation = stationRepository.findById(5);
        optionalStation.ifPresent(station -> stationRepository.delete(station));

        assertTrue(stationRepository.findById(5).isEmpty());
    }

}
