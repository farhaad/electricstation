package am.devolon.electric.station.service;

import am.devolon.electric.station.model.AllStationsRequest;
import am.devolon.electric.station.model.Company;
import am.devolon.electric.station.model.Station;
import am.devolon.electric.station.repository.CompanyRepository;
import am.devolon.electric.station.repository.StationRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.ArgumentMatchers;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.AssertionErrors;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

/**
 * created by farhad (farhad.yousefi@outlook.com) on 3/23/2021 AD
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest
public class CompanyServiceTest {
    @SpyBean
    private CompanyService companyService;

    @MockBean
    private StationRepository stationRepository;

    @MockBean
    private CompanyRepository companyRepository;

    @ParameterizedTest
    @CsvSource({"34.210,10.3,32", "35.210,11.3,30", "43.210,13.3,31"})
    public void TestFindAllStationsWithinRadius(Double longitude, Double latitude, Integer distance) {
        AllStationsRequest mockRequest = new AllStationsRequest(longitude, latitude, distance);
        mockRequest.setCompanyId(1);

        List<Station> mockStations = new ArrayList<>();
        mockStations.add(new Station(1, "station1", 11.2280839022311, 35.2108630836368, 1));
        mockStations.add(new Station(2, "station2", 12.2285534862982, 34.2107802226056, 1));
        Optional<Company> mockCompany = Optional.of(new Company(4, "D", "2", 1));

        doReturn(mockCompany).when(companyService).findCompanyById(anyInt());
        doReturn(mockStations).when(stationRepository).findAllInsideRadius(anyInt(), anyString(), anyDouble(), anyDouble(), anyInt());

        Optional<List<Station>> result = companyService.findAllStationsWithinRadius(mockRequest);
        assertTrue(result.isPresent());
        assertEquals(result.get(), mockStations);
        assertEquals(2L, mockStations.size());
    }


    @ParameterizedTest
    @CsvSource({"2", "3", "4"})
    @DisplayName("Find company with id successfully")
    public void testFindCompanyById(Integer id) {
        Optional<Company> mockCompany = Optional.of(new Company(id, "F", "4", 1));

        doReturn(mockCompany).when(companyRepository).findById(id);
        Optional<Company> optionalCompany = companyService.findCompanyById(id);

        assertTrue(optionalCompany.isPresent());
        assertEquals(optionalCompany.get(), mockCompany.get());
        Assertions.assertSame("F", optionalCompany.get().getName());
    }

    @ParameterizedTest
    @CsvSource({"20", "30", "40"})
    @DisplayName("Fail to find company with id")
    public void testFailToFindCompanyById(Integer id) {
        doReturn(Optional.empty()).when(companyRepository).findById(id);
        Optional<Company> optionalCompany = companyService.findCompanyById(id);
        assertTrue(optionalCompany.isEmpty());
    }

    @Test
    @DisplayName("Save new company successfully")
    public void testSuccessfulCompanySave() {
        Company newCompany = new Company(null, "K", "3", 1);
        Optional<Company> optionalCompany = Optional.of(new Company(3, "C", "1", 1));

        //mocking parent of company checking
        doReturn(optionalCompany).when(companyRepository).findById(anyInt());
        doReturn(10).when(companyRepository).saveCompany(anyString(), anyString(), anyInt());
        Company savedCompany = companyService.addCompany(newCompany);

        AssertionErrors.assertNotNull("company should not be null", savedCompany);
        Assertions.assertSame(10, savedCompany.getId());
        Assertions.assertSame("K", savedCompany.getName());
        Assertions.assertSame(1, savedCompany.getVersion());
    }


    @Test
    @DisplayName("Update an existing company successfully")
    public void testUpdatingCompanySuccessfully() {
        Optional<Company> companyToUpdate = Optional.of(new Company(2, "new B", "4", 1));

        Optional<Company> existCompany = Optional.of(new Company(2, "B", "1.3", 1));
        existCompany.get().setVersion(1);

        doReturn(existCompany).when(companyRepository).findById(anyInt());
        doReturn(2).when(companyRepository).updateCompany(anyString(), anyString(), anyInt(), anyInt());

        Optional<Company> updateCompany = companyService.updateCompany(existCompany.get(), companyToUpdate.get());

        assertTrue(updateCompany.isPresent());
        Assertions.assertEquals("new B", updateCompany.get().getName());
    }

    @Test
    @DisplayName("Delete a company successfully")
    public void testCompanyDeletedSuccessfully() {
        Company mockCompany = new Company(2, "B", "1", 1);

        List<Company> mockChildrenCompany = new ArrayList<>();
        mockChildrenCompany.add(new Company(4, "D", "2", 1));

        doReturn(mockChildrenCompany).when(companyRepository).findByParentCompanyId(anyString());
        doNothing().when(companyRepository).delete(ArgumentMatchers.any());

        List<Company> mockRemovedCompany = new ArrayList<>();
        doAnswer(e -> {
            Object[] arguments = e.getArguments();
            Company argument = (Company) arguments[0];
            mockRemovedCompany.add(argument);
            return null;
        }).when(companyRepository).delete(any());

        List<Company> removedCompanies = companyService.removeCompany(mockCompany);

        assertEquals(mockCompany, mockRemovedCompany.get(1));
        assertEquals(2, removedCompanies.size());
    }
}
