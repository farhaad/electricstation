package am.devolon.electric.station.service;

import am.devolon.electric.station.model.Company;
import am.devolon.electric.station.model.Station;
import am.devolon.electric.station.model.StationsRequest;
import am.devolon.electric.station.repository.StationRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;

/**
 * created by farhad (farhad.yousefi@outlook.com) on 3/23/2021 AD
 */

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class StationServiceTest {
    @Autowired
    private StationService stationService;

    @MockBean
    private StationRepository stationRepository;

    @MockBean
    private CompanyService companyService;

    @Test
    @DisplayName("Find station with id successfully")
    public void testFindStationById() {
        Optional<Station> mockStation = Optional.of(new Station(6, "station6", 11.2280839022311, 35.2108630836368, 1));

        doReturn(mockStation).when(stationRepository).findById(6);
        Optional<Station> optionalStation = stationService.findStationById(6);

        assertTrue(optionalStation.isPresent());
        assertEquals(optionalStation.get(), mockStation.get());
        Assertions.assertSame("station6", optionalStation.get().getName());
    }

    @Test
    @DisplayName("Save new station successfully")
    public void testSuccessfulStationSave() {
        StationsRequest mockCriteria = new StationsRequest(4, 3.2282642020913, 35.2107776350628, "station10");
        Optional<Company> mockCompany = Optional.of(new Company(4, "D", "2", 1));

        doReturn(mockCompany).when(companyService).findCompanyById(anyInt());
        doReturn(10).when(stationRepository).addStation(anyString(), anyDouble(), anyDouble(), anyInt(), anyInt());

        Optional<Station> savedStation = stationService.addStation(mockCriteria);

        assertTrue(savedStation.isPresent());
        Assertions.assertSame(10, savedStation.get().getId());
        Assertions.assertSame("station10", savedStation.get().getName());
        Assertions.assertSame(1, savedStation.get().getVersion());
    }

    @Test
    @DisplayName("Update an existing station successfully")
    public void testUpdatingStationSuccessfully() {
        StationsRequest mockCriteria = new StationsRequest(4, 3.2282642020913, 35.2107776350628, "update station10");
        Station mockStationToUpdate = new Station(4, "station10", 11.2280839022311, 35.2108630836368, 2);
        Optional<Company> mockCompany = Optional.of(new Company(4, "D", "2", 1));

        doReturn(mockCompany).when(companyService).findCompanyById(anyInt());
        doReturn(4).when(stationRepository).updateStation(anyString(), anyDouble(), anyDouble(), anyInt(), anyInt(), anyInt());

        Optional<Station> updateStation = stationService.updateStation(mockStationToUpdate, mockCriteria);

        assertTrue(updateStation.isPresent());
        Assertions.assertEquals("update station10", updateStation.get().getName());
    }

    @Test
    @DisplayName("Delete a station successfully")
    public void testStationDeletedSuccessfully() {
        Station mockStationToRemove = new Station(4, "station10", 11.2280839022311, 35.2108630836368, 2);
        doNothing().when(stationRepository).delete(ArgumentMatchers.any());
        stationService.removeStation(mockStationToRemove);

        assertTrue(stationService.findStationById(4).isEmpty());
    }


}
